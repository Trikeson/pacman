#pragma once
#include "Entity.h"
#include "Cell.h"
static const sf::Color PACKMAN_COLOR1 = sf::Color(246, 156, 31);
static const float PACKMAN_SPEED1 = 120.f; // pixels per second.
static const float PACKMAN_RADIUS1 = 16.f; // pixels

class Pacman1 : public MovingEntity
{
private:
    std::vector<Cell*> walls;
public:
    Pacman1(int xPos, int yPos) {
        direction = Direction::NONE;
        shape.setRadius(PACKMAN_RADIUS1);
        shape.setFillColor(PACKMAN_COLOR1);
        shape.setPosition(sf::Vector2f(xPos, yPos));
    }

    void setWalls(const std::vector<Cell*>& wallCells)
    {
        walls = wallCells;
    }

    void updatePacmanDirection() {
        direction = Direction::NONE;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
        {
            direction = Direction::UP;
        }
        else if ( sf::Keyboard::isKeyPressed(sf::Keyboard::S))
        {
            direction = Direction::DOWN;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
        {
            direction = Direction::LEFT;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
        {
            direction = Direction::RIGHT;
        }
    }

    void update(float elapsedTime, const std::vector<Cell*>& walls, std::vector<PacGum*>& pacGums) {
        const float step = PACKMAN_SPEED * elapsedTime;

        updatePacmanDirection();

        sf::Vector2f movement(0.f, 0.f);
        switch (direction)
        {
        case Direction::UP:
            movement.y -= step;
            break;
        case Direction::DOWN:
            movement.y += step;
            break;
        case Direction::LEFT:
            movement.x -= step;
            break;
        case Direction::RIGHT:
            movement.x += step;
            break;
        case Direction::NONE:
            break;
        }
        const sf::FloatRect packman1Bounds = shape.getGlobalBounds();
        sf::FloatRect nextBounds = shape.getGlobalBounds();
        nextBounds.left += movement.x;
        nextBounds.top += movement.y;
        for (const auto& wall : walls)
        {
            if (wall->getBounds().intersects(nextBounds))
            {
                // Произошло столкновение со стеной, останавливаем пакман
                movement = sf::Vector2f(0.f, 0.f);
                break;
            }
        }
        shape.move(movement);


    }


};

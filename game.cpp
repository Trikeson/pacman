#include "game.h"
#include "Entity.h"
#include "Cell.h"
#include <iostream>


Game::Game() {
    width = 0;
    height = 0;
    pacman = new Pacman(190, 40);
    pacman1 = new Pacman1(220, 40);
    blinky = new Blinky(880, 40);
    clyde = new Clyde(880, 930);
    inky = new Inky(120, 920);
    pinky = new Pinky(120, 40);
    createWalls(); // Вызов метода createWalls() для создания стенок

}

void Game::updateGame(float elapsedTime) {
    pacman->update(elapsedTime, cells, pacGums);
    pacman1->update(elapsedTime, cells, pacGums);
    blinky->update(elapsedTime, cells);
    inky->update(elapsedTime, cells);
    pinky->update(elapsedTime, cells);
    clyde->update(elapsedTime, cells);

    for (auto it = pacGums.begin(); it != pacGums.end(); )
    {
        PacGum* pacGum = *it;
        sf::FloatRect pacmanBounds = pacman->getBounds();
        sf::FloatRect pacman1Bounds = pacman1->getBounds();
        sf::CircleShape pacGumShape(pacGum->size / 2.0f);
        pacGumShape.setPosition(sf::Vector2f(pacGum->xPos, pacGum->yPos));
        sf::FloatRect pacGumBounds = pacGumShape.getGlobalBounds();
        if (pacGumBounds.contains(pacmanBounds.left + pacmanBounds.width / 2.0f, pacmanBounds.top + pacmanBounds.height / 2.0f)
            || pacGumBounds.contains(pacman1Bounds.left + pacman1Bounds.width / 2.0f, pacman1Bounds.top + pacman1Bounds.height / 2.0f))
        {
            it = pacGums.erase(it);
            objects.erase(std::find(objects.begin(), objects.end(), pacGum));
            delete pacGum;
            // Увеличение счета игры
            score += 10;
            std::cout << "\n" << score;
        }
        else
        {
            ++it;
        }
    }



    for (auto it = SuperpacGums.begin(); it != SuperpacGums.end(); )
    {
        SuperPacGum* SuperpacGum = *it;
        sf::FloatRect pacmanBounds = pacman->getBounds();
        sf::FloatRect pacman1Bounds = pacman1->getBounds();
        sf::CircleShape pacGumShape(SuperpacGum->size / 2.0f);
        pacGumShape.setPosition(sf::Vector2f(SuperpacGum->xPos, SuperpacGum->yPos));
        sf::FloatRect pacGumBounds = pacGumShape.getGlobalBounds();
        if (pacGumBounds.contains(pacmanBounds.left + pacmanBounds.width / 2.0f, pacmanBounds.top + pacmanBounds.height / 2.0f)
            || pacGumBounds.contains(pacman1Bounds.left + pacman1Bounds.width / 2.0f, pacman1Bounds.top + pacman1Bounds.height / 2.0f))
        {
            it = SuperpacGums.erase(it);
            objects.erase(std::find(objects.begin(), objects.end(), SuperpacGum));
            delete SuperpacGum;
            // Увеличение счета игры
            score += 100;
            std::cout << "\n" << score;
        }
        else
        {
            ++it;
        }
    }
}

void Game::render(sf::RenderWindow& window) const {
    window.clear();

    sf::FloatRect pacmanBounds = pacman->getBounds();
    sf::FloatRect pacman1Bounds = pacman1->getBounds();
    sf::FloatRect pinkyBounds = pinky->getBounds();
    sf::FloatRect inkyBounds = inky->getBounds();
    sf::FloatRect clydeBounds = clyde->getBounds();
    sf::FloatRect blinkyBounds = blinky->getBounds();

    if (pinkyBounds.intersects(pacmanBounds) || pinkyBounds.intersects(pacman1Bounds) ||
        inkyBounds.intersects(pacmanBounds) || inkyBounds.intersects(pacman1Bounds) ||
        clydeBounds.intersects(pacmanBounds) || clydeBounds.intersects(pacman1Bounds) ||
        blinkyBounds.intersects(pacmanBounds) || blinkyBounds.intersects(pacman1Bounds))
    {
        // Здесь может быть код для обработки окончания игры при столкновении с привидениями
        // Например, вызов функции, которая завершает игру или отображает сообщение о поражении.
        // Например:
        window.close();
    }

    // Отрисовываем Пакмана
    pacman->render(window);
    pacman1->render(window);
    blinky->render(window);
    inky->render(window);
    pinky->render(window);
    clyde->render(window);
    // Отрисовка стенок
    for (const auto& cell : cells)
    {
        cell->render(window);
    }

    // Отрисовка остальных объектов
    for (const auto& object : objects)
    {
        object->render(window);
    }

    // Отрисовка печенек
    for (const auto& pacGum : pacGums)
    {
        pacGum->render(window);
    }
}

void Game::createWalls()
{
    std::string levelMap =
        " ####################### "
        " #0000000000#0000000000# "
        " #0##0#####0#0#####0##0# "
        " #000000000000000000000# "
        " #0##0#0#########0#0##0# "
        " #0000#00000#00s00#0000# "
        " ####0#####0#0#####0#### "
        "    #0#00000000000#0#    "
        "#####0#0#0#####0#0#0#####"
        "#0000000#0#   #0#0000000#"
        "#####0#0#0#####0#0#0#####"
        "    #0#00000000000#0#    "
        " ####0#0#########0#0#### "
        " #0000000000#0000000000# "
        " #0##s#####0#0#####0##0# "
        " #0#0000000000000000#00# "
        " ##0#0#0#########0#0#00# "
        " #0000#00000#00000#0000# "
        " #0#######0###0#######0# "
        " #0#00000#00000#0s000#0# "
        " #0#0###0##0#0##0###0#0# "
        " #0#0#000000#000000#0#0# "
        " #0#0#0####0#0####0#0#0# "
        " #0000000000#0000000000# "
        " ####################### ";;

    const int wallSize = 40;
    const int levelWidth = 25;  // Ширина уровня (количество символов в строке)
    const int levelHeight = 25;  // Высота уровня (количество строк)

    for (int y = 0; y < levelHeight; ++y)
    {
        for (int x = 0; x < levelWidth; ++x)
        {
            char symbol = levelMap[y * levelWidth + x];
            if (symbol == '#')
            {
                Cell* wall = new Cell(x * wallSize, y * wallSize, wallSize);
                cells.push_back(wall);
                objects.push_back(wall);
            }
            else if (symbol == '0')
            {
                PacGum* pacGum = new PacGum((x-0.1f) * wallSize + wallSize / 2, (y-0.1f) * wallSize + wallSize / 2, 8);
                pacGums.push_back(pacGum);
                objects.push_back(pacGum);
            }
            else if (symbol == 's')
            {
                SuperPacGum* SuperpacGum = new SuperPacGum((x - 0.1f) * wallSize + wallSize / 2, (y - 0.1f) * wallSize + wallSize / 2, 8);
                SuperpacGums.push_back(SuperpacGum);
                objects.push_back(SuperpacGum);
            }
        }
    }
    objects.insert(objects.end(), cells.begin(), cells.end());
}

#pragma once
#include <vector>
#include <SFML/Graphics.hpp>
#include "pacman.h"
#include "Entity.h"
#include "Cell.h"
#include "ghost.h"
#include "SecondPlayer.h"

/*enum class GameState
{
	Playing,
	Lose,
}; */

class Game
{
	int width;
	int height;
	std::vector<Entity*> objects;
	//std::vector<Ghost*> ghosts;
	std::vector<Cell*> cells;
	std::vector<PacGum*> pacGums; 
	std::vector<SuperPacGum*> SuperpacGums;
	int score;
	Pacman* pacman;
	Pacman1* pacman1;
	Blinky* blinky;
	Clyde* clyde;
	Inky* inky;
	Pinky* pinky;
	//GameState gamestate = GameState::Playing;

public:

	Game();
	std::vector<Entity*> getEntities();
	std::vector<Cell*> getWalls();
	Blinky* getBlinky();
	Pinky* getPinky();
	Inky* getInky();
	Clyde* getClyde();
	Pacman* getPacman();
	Pacman1* getPacman1();
	void updateGame(float elapsedTime); // изменения координат объектов
	void render(sf::RenderWindow& window) const; // отрисовка
	void createWalls();
	int getScore() { return score; }
	~Game() = default;

};

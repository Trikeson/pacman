#pragma once
#include "Entity.h"
#include "Cell.h"
#include "pacman.h"
static const sf::Color GHOST_COLOR = sf::Color(200, 32, 200);
static const sf::Color GHOST_COLOR1 = sf::Color(100, 32, 200);
static const sf::Color GHOST_COLOR2 = sf::Color(200, 32, 100);
static const sf::Color GHOST_COLOR3 = sf::Color(200, 132, 200);
static const sf::Color GHOST_COLOR4 = sf::Color(200, 232, 200);
static const float GHOST_SPEED = 60.f; // pixels per second.
static const float GHOST_RADIUS = 16.f; // pixels

class Ghost : public MovingEntity
{
private:
    std::vector<Cell*> walls;
public:
    Ghost(int xPos, int yPos) {
        direction = Direction::NONE;
        shape.setRadius(GHOST_RADIUS);
        shape.setFillColor(GHOST_COLOR);
        shape.setPosition(sf::Vector2f(xPos, yPos));
    }

    void setWalls(const std::vector<Cell*>& wallCells)
    {
        walls = wallCells;
    }

    void updateGhostDirection() {
        switch (direction)
        {
        case Direction::UP:
            direction = Direction::DOWN;
            break;
        case Direction::DOWN:
            direction = Direction::LEFT;
            break;
        case Direction::LEFT:
            direction = Direction::RIGHT;
            break;
        case Direction::RIGHT:
            direction = Direction::NONE;
            break;
        case Direction::NONE:
            direction = Direction::UP;
            break;
        }
    }

    void update(float elapsedTime, const std::vector<Cell*>& walls) {
        const float step = GHOST_SPEED * elapsedTime;

        sf::Vector2f movement(0.f, 0.f);
        switch (direction)
        {
        case Direction::UP:
            movement.y -= step;
            break;
        case Direction::DOWN:
            movement.y += step;
            break;
        case Direction::LEFT:
            movement.x -= step;
            break;
        case Direction::RIGHT:
            movement.x += step;
            break;
        case Direction::NONE:
            updateGhostDirection();
            break;
        }
        const sf::FloatRect ghostBounds = shape.getGlobalBounds();
        sf::FloatRect nextBounds = shape.getGlobalBounds();
        nextBounds.left += movement.x;
        nextBounds.top += movement.y;
        for (const auto& wall : walls)
        {
            if (wall->getBounds().intersects(nextBounds))
            {
                // Если есть коллизия, отменяем движение пакмана
                shape.move(-movement);
                
                switch (direction)
                {
                case Direction::UP:
                    updateGhostDirection();
                    break;
                case Direction::DOWN:
                    updateGhostDirection();
                    break;
                case Direction::LEFT:
                    updateGhostDirection();
                    break;
                case Direction::RIGHT:
                    updateGhostDirection();
                    break;
                case Direction::NONE:
                    break;
                }
            }
        }
        shape.move(movement);

    }
};

class Blinky : public Ghost {
public:
    Blinky(int xPos, int yPos) : Ghost(xPos, yPos) {
        // Конструктор класса Blinky
        shape.setFillColor(GHOST_COLOR1);
    }
};
class Clyde : public Ghost {
public:
    Clyde(int xPos, int yPos) : Ghost(xPos, yPos) {
        // Конструктор класса Clyde
        shape.setFillColor(GHOST_COLOR2);
    }
};
class Inky : public Ghost {
public:
    Inky(int xPos, int yPos) : Ghost(xPos, yPos) {
        // Конструктор класса Inky
        shape.setFillColor(GHOST_COLOR3);
    }
};
class Pinky : public Ghost {
public:
    Pinky(int xPos, int yPos) : Ghost(xPos, yPos) {
        // Конструктор класса Pinky
        shape.setFillColor(GHOST_COLOR4);
    }
};
